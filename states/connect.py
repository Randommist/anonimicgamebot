from aiogram.dispatcher.filters.state import StatesGroup, State

class Connection(StatesGroup):
    Enter = State()
    Success = State()