from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

menu_buttons = ReplyKeyboardMarkup(
    [
        [
            "Присоединиться", "Создать комнату"
        ]
    ],
    resize_keyboard=True
)