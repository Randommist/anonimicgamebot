
from random import shuffle

from aiogram import types
from aiogram.dispatcher.filters.builtin import CommandStart, Command
from aiogram.dispatcher.storage import FSMContext

from loader import dp

from keyboards.default import menu, menu_buttons, create_buttons, connection_buttons, chatting_buttons
from keyboards.inline import choice_callback

from states import LobbyCreate
from states import Connection
from states import InGame

from game import Lobby, Player, lobbys


def find_lobby(user_id: int) -> Lobby:
    for l in lobbys:
        for p in l.Players:
            if p.self_id == user_id:
                return l
    return None

def find_player(user_id: int) -> Player:
    for l in lobbys:
        for p in l.Players:
            if p.self_id == user_id:
                return p
    return None


@dp.message_handler(Command("deb"), state= "*")
async def debug(message: types.Message, state: FSMContext):
    print("\n")
    print(f"Lobbys count: {len(lobbys)}")
    num = 0
    for l in lobbys:
        num+=1
        print(f"    Lobby {num}")
        print(f"    {l.PlayersNames}")
        for p in l.Players:
            print(f"        {p.name} {p.self_id} {p.destination_id} state= {await state.storage.get_state(user=p.self_id)}")

#Создание комнаты
@dp.message_handler(CommandStart())
async def bot_start(message: types.Message):
    await message.answer(f'Добро пожаловать в игру Anonimic!\nМожете создать комнату или присоединиться к уже существующей', reply_markup=menu_buttons)

@dp.message_handler(text="Создать комнату")
async def create_lobby(message: types.Message):
    await LobbyCreate.Preparation.set()
    lobby = Lobby(list())
    lobby.Players.append(Player(message.from_user.first_name, message.from_user.id, None))
    lobbys.append(lobby)
    await message.answer(f"Комната создана. ID комнаты - {lobby.lobby_id}\nИгроков: {len(lobby.Players)}", reply_markup=create_buttons)

@dp.message_handler(text= "Выйти", state= LobbyCreate.Preparation)
async def exit_lobby_creation(message: types.Message, state: FSMContext):
    await message.answer("Вы вышли. Комната была удалена", reply_markup= menu_buttons)
    await state.finish()

    #Оповестим всех что мы вышили =)
    for l in lobbys:
        lobby = None
        for p in l.Players:
            if p.self_id == message.from_user.id:
                l.Players.remove(p)
                lobby = l
                break
        if lobby == None: continue

        for p in lobby.Players:
            await dp.bot.send_message(p.self_id, f"Комната удалена. Создатель комнаты вышел.")
        lobbys.remove(lobby)
        break

@dp.message_handler(text="Начать", state= LobbyCreate.Preparation)
async def start(message: types.Message, state: FSMContext):
    lobby = find_lobby(message.from_user.id)
    if len(lobby.Players) < 2 or len(lobby.Players) % 2 != 0 or len(lobby.Players) > 16:
        await message.answer("Не подходящие число игроков. Игроков должно быть чётное количество от 2 до 16")
        return
    check = False
    for i in range(len(lobby.Players)):
        for g in range(i+1, len(lobby.Players), 1):
            if lobby.Players[i].name == lobby.Players[g].name:
                await message.answer("В комнате есть игроки с одинаковыми именами. Пускай один из игроков с одинаковым именем выйдет из комнаты и поменяет имя и после этого заёдет")
                return
                


    for p in lobby.Players:
        lobby.PlayersNames.append(p.name)
    shuffle(lobby.Players)
    for i in range(int(len(lobby.Players) / 2)):
        lobby.Players[i*2].destination_id = lobby.Players[i*2+1].self_id
        lobby.Players[i*2+1].destination_id = lobby.Players[i*2].self_id
    for p in lobby.Players:
        await state.storage.reset_state(user=p.self_id)
        await state.storage.set_state(user=p.self_id, state= InGame.Play)
        await dp.bot.send_message(p.self_id, "*_____ИГРА НАЧАЛАСЬ_____*", reply_markup= chatting_buttons, parse_mode="Markdown")
        

#Подключение к комнате
@dp.message_handler(text="Присоединиться")
async def connect(message: types.Message):
    await message.answer("Введите ID комнаты", reply_markup= connection_buttons)
    await Connection.Enter.set()

@dp.message_handler(state=Connection.Enter)
async def connect_try(message: types.Message, state: FSMContext):
    if message.text == "Выйти":
        await state.finish()
        await message.answer("Вы вышили", reply_markup= menu_buttons)
        return
    isFound = False
    lobby = None
    for l in lobbys:
        if l.lobby_id == int(message.text):
            isFound = True
            l.Players.append(Player(message.from_user.first_name, message.from_user.id, None))
            lobby = l
            break
    if isFound == False:
        await message.answer("Такой комнаты не существует")
    else:
        await message.answer(f"Вы успешно подключились\nИгроков: {len(lobby.Players)}", reply_markup= connection_buttons)
        await Connection.Success.set()
        #Оповести всех остальных что вы подключились
        for p in lobby.Players:
            if p.self_id != message.from_user.id: await dp.bot.send_message(p.self_id, f"Игроков: {len(lobby.Players)}")


@dp.message_handler(text= "Выйти", state=Connection.Success)
async def exit_connect(message: types.Message, state: FSMContext):
    await message.answer("Вы вышли", reply_markup= menu_buttons)
    await state.finish()

    #Оповестим всех что мы вышили =)
    for l in lobbys:
        lobby = None
        for p in l.Players:
            if p.self_id == message.from_user.id:
                l.Players.remove(p)
                lobby = l
                break
        if lobby == None: continue

        for p in lobby.Players:
            await dp.bot.send_message(p.self_id, f"Игроков: {len(lobby.Players)}")
        break

@dp.message_handler(text= "Выйти", state=InGame.Play)
async def exit_game(message: types.Message, state: FSMContext):
    player = find_player(message.from_user.id)
    player_dist = find_player(player.destination_id)
    await dp.bot.send_message(player.destination_id, "*_____ИГРОК ВЫШЕЛ_____*", reply_markup= menu_buttons, parse_mode="Markdown")
    await message.answer("*_____ВЫ ВЫШЛИ ИЗ ИГРЫ_____*", reply_markup= menu_buttons,parse_mode="Markdown")
    await state.storage.reset_state(user= player.destination_id)
    await state.finish()
    lobby = find_lobby(message.from_user.id)
    lobby.Players.remove(player)
    lobby.Players.remove(player_dist)
    if len(lobby.Players) == 0: lobbys.remove(lobby)


@dp.message_handler(text= "Выбрать", state=InGame.Play)
async def exit_game(message: types.Message, state: FSMContext):
    lobby = find_lobby(message.from_user.id)
    buttons = list[list[types.InlineKeyboardButton]()]()
    for n in lobby.PlayersNames:
        cb_player_id = None
        for p in lobby.Players:
            if n == p.name:
                cb_player_id = p.self_id
                break
        if cb_player_id != message.from_user.id:
            buttons.append([types.InlineKeyboardButton(text=n, callback_data=choice_callback.new(player_id= str(cb_player_id), player_name= str(n)))])
    keyboard = types.InlineKeyboardMarkup(row_width=1, inline_keyboard=buttons)
    await message.answer("*Выберете имя*",reply_markup= keyboard, parse_mode="Markdown")
    return

@dp.callback_query_handler(choice_callback.filter(), state=InGame.Play)
async def callback_name_choice(call : types.CallbackQuery, callback_data: dict, state: FSMContext):
    player = find_player(call.from_user.id)
    if str(player.destination_id) == callback_data.get("player_id"):
        await call.message.answer("*_____ВЫ ПОБЕДИЛИ_____*", reply_markup= menu_buttons, parse_mode="Markdown")
        await dp.bot.send_message(player.destination_id, "*_____ВЫ ПРОИГРАЛИ_____*", reply_markup= menu_buttons, parse_mode="Markdown")
    else:
        await call.message.answer("*_____ВЫ ПРОИГРАЛИ_____*", reply_markup= menu_buttons, parse_mode="Markdown")
        await dp.bot.send_message(player.destination_id, "*_____ВЫ ПОБЕДИЛИ_____*", reply_markup= menu_buttons, parse_mode="Markdown")
    await call.answer(cache_time=60)
    await state.storage.reset_state(user= player.destination_id)
    await state.finish()
    lobby = find_lobby(call.from_user.id)
    lobby.Players.remove(player)
    player_dist = find_player(player.destination_id)
    lobby.Players.remove(player_dist)
    if len(lobby.Players) == 0: lobbys.remove(lobby)
    

@dp.message_handler(state=InGame.Play)
async def recive_message_in_game(message: types.Message, state: FSMContext):
    player = find_player(message.from_user.id)
    await dp.bot.send_message(player.destination_id, f"*anonim:* {message.text}", parse_mode="Markdown")
    return