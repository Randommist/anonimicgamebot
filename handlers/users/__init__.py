from .help import dp
from .start import dp
from .type import dp
from .echo import dp

__all__ = ["dp"]
