from itertools import count
import re
from aiogram import types
from aiogram.dispatcher.filters.builtin import Command

from loader import dp

@dp.message_handler(Command('type'))
async def cmd_type(message: types.Message):
    if len(message.text.split()) < 3:
        return
    id = message.text.split()[1]
    mes = ""
    count_bs = 0
    for i in message.text:
        if i == ' ': count_bs += 1
        if count_bs >= 2: mes += i
    await dp.bot.send_message(chat_id=id, text=mes)
