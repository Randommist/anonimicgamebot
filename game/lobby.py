from random import Random
import random
from game import Player

class Lobby():
    def __init__(self, Players: list[Player]) -> None:
        self.Players = Players 
        self.lobby_id = random.randint(1000, 9999)
        self.PlayersNames = list[str]()
        